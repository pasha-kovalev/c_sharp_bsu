﻿using System;
using System.Collections.Generic;
using BookClass;
using NUnit.Framework;

#pragma warning disable CA1707 // Identifiers should not contain underscores
#pragma warning disable SA1600 // Elements should be documented

namespace BookListService.Tests
{
    [TestFixture]
    public class BookListServiceTests
    {
        [TestCase("Stephen King", "It", "Simon&Shuster")]
        public void BookService_Add_Book(string author, string title, string publisher)
        {
            BookListService bookListService = new BookListService();
            Book book = new Book(author, title, publisher);

            bookListService.Add(book);
            Assert.IsFalse(bookListService.isEmpty());
        }

        [TestCase("Stephen King", "It", "Simon&Shuster")]
        public void BookService_Add_Same_Book(string author, string title, string publisher)
        {
            BookListService bookListService = new BookListService();
            Book book = new Book(author, title, publisher);

            bookListService.Add(book);
            Assert.Throws<Exception>(() => bookListService.Add(book), "This book has already been added.");
        }

        [TestCase("Stephen King", "It", "Simon&Shuster")]
        public void BookService_Remove_Book(string author, string title, string publisher)
        {
            BookListService bookListService = new BookListService();
            Book book = new Book(author, title, publisher);

            bookListService.Add(book);
            bookListService.Remove(book);
            Assert.IsTrue(bookListService.isEmpty());
        }

        [TestCase("Stephen King", "It", "Simon&Shuster")]
        public void BookService_Remove_Unexisting_Book(string author, string title, string publisher)
        {
            BookListService bookListService = new BookListService();
            Book book = new Book(author, title, publisher);
            bookListService.Add(new Book(string.Empty, string.Empty, string.Empty));

            Assert.Throws<Exception>(() => bookListService.Remove(book), "There is no such book.");
        }

        [TestCase("Stephen King")]
        public void BookService_FindByAuthor(string author)
        {
            BookListService bookListService = new BookListService();

            Book book1 = new Book("Stephen King", "The Shawshank Redemption",
                "Horror Stories");
            Book book2 = new Book(author, "The Shining", "Horror Stories");
            Book book3 = new Book("Abobus", "Title","Publisher");

            bookListService.Add(book1);
            bookListService.Add(book3);
            bookListService.Add(book2);

            List<Book> list = bookListService.FindBy(book => book.Author.Equals(author));

            Assert.IsTrue(list.Contains(book2) && list.Contains(book1) && list.Count == 2);
        }

        [TestCase("It")]
        public void BookService_FindByTitle(string title)
        {
            BookListService bookListService = new BookListService();

            Book book1 = new Book(string.Empty, string.Empty, "Simon&Shuster");
            Book book2 = new Book("Stephen King", string.Empty, string.Empty);
            Book book3 = new Book(string.Empty, title, string.Empty);
            Book book4 = new Book(string.Empty, string.Empty, string.Empty);
            Book book5 = new Book("Stephen King", title, string.Empty);

            bookListService.Add(book1);
            bookListService.Add(book2);
            bookListService.Add(book3);
            bookListService.Add(book4);
            bookListService.Add(book5);

            List<Book> list = bookListService.FindByTitle(title);

            Assert.IsTrue(list.Contains(book3) && list.Contains(book5) && list.Count == 2);
        }

        [TestCase("Simon&Shuster")]
        public void BookService_FindByPublisher(string publisher)
        {
            BookListService bookListService = new BookListService();

            Book book1 = new Book(string.Empty, string.Empty, publisher);
            Book book2 = new Book("Stephen King", string.Empty, string.Empty);
            Book book3 = new Book(string.Empty, "It", string.Empty);
            Book book4 = new Book(string.Empty, string.Empty, string.Empty);
            Book book5 = new Book("Stephen King", "It", string.Empty);

            bookListService.Add(book1);
            bookListService.Add(book2);
            bookListService.Add(book3);
            bookListService.Add(book4);
            bookListService.Add(book5);

            List<Book> list = bookListService.FindByPublisher(publisher);

            Assert.IsTrue(list.Contains(book1) && list.Count == 1);
        }

        [TestCase("Cdoba", "Aboba", "Boba")]
        public void BookService_GetByAuthor(string author1, string author2, string author3)
        {
            BookListService bookListService = new BookListService();

            Book book1 = new Book(author1, string.Empty, string.Empty);
            Book book2 = new Book(author2, string.Empty, string.Empty);
            Book book3 = new Book(author3, string.Empty, string.Empty);
   
            bookListService.Add(book1);
            bookListService.Add(book2);
            bookListService.Add(book3);

            List<Book> list = bookListService.GetBy(new BookAuthorComparator());

            Assert.IsTrue(list[0] == book2 && list[1] == book3 && list[2] == book1);
        }

        [TestCase(478, 66, 327)]
        public void BookService_GetByPages(int pages1, int pages2, int pages3)
        {
            BookListService bookListService = new BookListService();

            Book book1 = new Book("Aboba", string.Empty, string.Empty) { Pages = pages1 };
            Book book2 = new Book("Boba", string.Empty, string.Empty) { Pages = pages2 };
            Book book3 = new Book("Cdoba", string.Empty, string.Empty) { Pages = pages3 };

            bookListService.Add(book1);
            bookListService.Add(book2);
            bookListService.Add(book3);

            List<Book> list = bookListService.GetByPages();

            Assert.IsTrue(list[0] == book2 && list[1] == book3 && list[2] == book1);
        }

        [TestCase(155, 99, 111, "RUB")]
        public void BookService_GetByPrice(int price1, int price2, int price3, string currency)
        {
            BookListService bookListService = new BookListService();

            Book book1 = new Book("Aboba", string.Empty, string.Empty);
            Book book2 = new Book("Boba", string.Empty, string.Empty);
            Book book3 = new Book("Cdoba", string.Empty, string.Empty);

            book1.SetPrice(price1, currency);
            book2.SetPrice(price2, currency);
            book3.SetPrice(price3, currency);

            bookListService.Add(book1);
            bookListService.Add(book2);
            bookListService.Add(book3);

            List<Book> list = bookListService.GetByPrice();

            Assert.IsTrue(list[0] == book2 && list[1] == book3 && list[2] == book1);
        }

        [Test]
        public void BookService_Load_From_BookStorage()
        {
            BookListService bookListService = new BookListService();

            Book book1 = new Book("Aboba", string.Empty, string.Empty);
            Book book2 = new Book("Boba", string.Empty, string.Empty);
            Book book3 = new Book("Cdoba", string.Empty, string.Empty);
            FakeBookStorage bookStorage = new FakeBookStorage(new List<Book>() {book1, book2, book3});

            bookListService.Load(bookStorage);
            var bookList = bookListService.GetBookList();

            Assert.IsTrue(bookList.ContainsValue(book1) && bookList.ContainsValue(book2) && bookList.ContainsValue(book3) && bookList.Count == 3);
        }

        [Test]
        public void BookService_Save_Into_BookStorage()
        {
            BookListService bookListService = new BookListService();

            Book book1 = new Book("Aboba", string.Empty, string.Empty);
            Book book2 = new Book("Boba", string.Empty, string.Empty);
            Book book3 = new Book("Cdoba", string.Empty, string.Empty);

            bookListService.Add(book1);
            bookListService.Add(book2);
            bookListService.Add(book3);

            FakeBookStorage bookStorage = new FakeBookStorage();

            bookListService.Save(bookStorage);
            
            Assert.IsFalse(bookStorage.isEmpty());
        }
    }
}
