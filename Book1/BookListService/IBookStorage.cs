﻿using System.Collections.Generic;
using BookClass;

namespace BookListService
{
    public interface IBookStorage
    {
        bool isEmpty();
        List<Book> GetBooks();
        void AddBooks(List<Book> books);
    }
}
